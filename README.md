# Git2Neo4J

Git2Neo4J is a command-line tool that loads a git repository's commit history into a Neo4J database.

It answers the question:
> Which pairs of files in my codebase occur together in the same commits the most often?

## Prerequisites

- Java 7
- Neo4j 2.0.0-M06 running on localhost:7474
- Sbt

## Quick start

1. Build it:

```
$ sbt assembly
```

2. Run it without arguments to get what help there is:

```
$ ./sbin/git2neo4j.sh
```

3. Load the latest commits from the Git repository at the specified location into Neo4J:

```
$ ./sbin/git2neo4j.sh load -g '/Users/johnmuth/projects/core/.git'
```

4. List the top co-committed files in the Git repository at the specified location:

```
$ ./sbin/git2neo4j.sh top-co-commits -g '/Users/johnmuth/projects/core/.git'
```

## Mingle integration

Git2Neo4J can optionally add data from Mingle to the graph.

My team and I are using [Mingle](http://www.thoughtworks.com/products/mingle-agile-project-management) for project management.  We try to remember to include a hashtag with the Mingle card number of the story or bug we're working on when we make Git commits.

The "load" command takes an optional argument, "--mingle-api-url", which cause Git2Neo4J to create a node for each Mingle card, typed as "Story" or "Bug", and associated with the relevant commits. Then we can answer questions like,
> Which files are associated most often with 'Bug' Mingle cards?

Supply the base URL for a Mingle API like so:

```
$ ./sbin/git2neo4j.sh load -g '/Users/johnmuth/projects/core/.git' -m 'http://mingle.tools.springer-sbm.com:8080/api/v2'
```

