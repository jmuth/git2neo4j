name := "Git2Neo4J"

organization := "com.springer"

version := "0.0.1"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "1.9.2" % "test",
  "org.anormcypher" %% "anormcypher" % "0.4.3",
  "org.rogach" %% "scallop" % "0.9.4",
  "org.apache.httpcomponents" % "httpclient" % "4.3.1",
  "org.apache.httpcomponents" % "httpcore" % "4.3",
  "org.eclipse.jgit" % "org.eclipse.jgit" % "3.0.0.201306101825-r",
  "org.mockito" % "mockito-all" % "1.9.5" % "test",
  "org.slf4j" % "slf4j-simple" % "1.6.4"
)

resolvers ++= Seq(
  "anormcypher" at "http://repo.anormcypher.org/",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
)

initialCommands := "import com.springer.git2neo4j._"

