package neo4j

import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSpec}
import git.{Repository}
import org.scalatest.mock.MockitoSugar
import org.scalatest.matchers.ShouldMatchers
import model.{FileChange, Commit}

class DBSpec extends FunSpec with MockitoSugar with BeforeAndAfterEach with BeforeAndAfterAll with ShouldMatchers {

  override def beforeEach() {
    DB.clean()
  }

  override def afterAll() {
    DB.clean()
  }

  describe("A Neo4J DB") {
    it("should find top co-commits"){
      val repo = Repository("/home/joeblogs/projects/funproject1/.git")
      val commit1 = Commit(
        "90900909090990",
        1383214886,
        "#FixingBuild this time it's personal",
        Seq(
          FileChange("MODIFY", "src/abc.txt", "src/abc.txt"),
          FileChange("MODIFY", "src/def.txt", "src/def.txt")
        )
      )
      val commit2 = Commit(
        "88888888888888",
        1383214887,
        "#999 enbetterment",
        Seq(
          FileChange("MODIFY", "src/abc.txt", "src/abc.txt"),
          FileChange("MODIFY", "src/def.txt", "src/def.txt"),
          FileChange("MODIFY", "src/ghi.txt", "src/ghi.txt")
        )
      )
      DB.createRepo(repo)
      DB.create(repo, commit1)
      DB.create(repo, commit2)
      DB.topCoCommits(repo)(0).files should be(("src/def.txt", "src/abc.txt"))
      DB.topCoCommits(repo)(0).count should be(2)
    }
  }

}

