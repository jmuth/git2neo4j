package mingle

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers._
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito.when
import org.mockito.Matchers.any
import org.apache.http.client.methods.{CloseableHttpResponse, HttpUriRequest}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import model.Commit

class MingleSpec extends FunSpec with MockitoSugar {
  describe("A Mingle") {
    it("should return a card"){
      val httpClient = mock[DefaultHttpClient]
      val response = mock[CloseableHttpResponse]
      new StringEntity("")
      val fakeCardName = "This is a card name"
      val fakeCardType = "Story"
      val fakeCardXml = <cardXml><name>{fakeCardName}</name><card_type><name>{fakeCardType}</name></card_type></cardXml>
      when(response.getEntity).thenReturn(new StringEntity(fakeCardXml.toString()))
      when(httpClient.execute(any[HttpUriRequest])).thenReturn(response )
      val mingle = new Mingle(httpClient)
      val commit = mock[Commit]
      when(commit.hashTags).thenReturn(Seq("#5083"))
      val card = mingle.cardsFor(Seq(commit)).get(commit).get
      card.id should be(5083)
      card.title should be(fakeCardName)
      card.cardType should be(fakeCardType)
    }
  }
}

