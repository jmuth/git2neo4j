package git

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers._

class RepositorySpec extends FunSpec {
  describe("A Git repository") {
    it("should return the latest n commits"){
      val repo = Repository("./.git")
      val commits = repo.latestCommits(3)
      commits.size should be(3)
    }
  }
}

