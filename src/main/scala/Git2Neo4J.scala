import git.Repository
import mingle.Mingle
import model.{Card, Commit}
import neo4j.DB
import org.rogach.scallop.{Subcommand, ScallopConf}
import org.slf4j.LoggerFactory

object Git2Neo4J {
  val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]) = {
    object Conf extends ScallopConf(args) {
      val load = new Subcommand("load") {
        val gitRepo = opt[String]("git-repo", descr = "Local Git repository .git directory", argName = "dir", required=true)
        val mingleApiUrl = opt[String]("mingle-api-url", descr = "Mingle API base url, e.g., http://mingle.tools.springer-sbm.com:8080/api/v2", argName = "url", required=false)
        val maxCommits = opt[Int]("max-commits", default=Some(100), required=false, descr="Number of commits to load, starting from the most recent. ")
        val maxChangesPerCommit = opt[Int]("max-changes-per-commit", default=Some(50), required=false, descr="Ignore commits with more than N changes.")
      }
      val topCoCommits = new Subcommand("top-co-commits") {
        val gitRepo = opt[String]("git-repo", descr = "Local Git repository .git directory", argName = "dir", required=true)
        val maxResults = opt[Int]("max-results", default=Some(20), required=false)
      }
      val topByTag= new Subcommand("top-by-tag") {
        val gitRepo = opt[String]("git-repo", descr = "Local Git repository .git directory", argName = "dir", required=true)
        val tag = opt[String]("tag", descr = "tag (#hashtag in commit messages)", required=true)
        val maxResults = opt[Int]("max-results", default=Some(20), required=false)
      }
      val clean = new Subcommand("clean")
    }
    Conf.subcommand match {
      case Some(Conf.topCoCommits) if (Conf.topCoCommits.gitRepo.isSupplied)=> {
        DB.topCoCommits(Repository(Conf.topCoCommits.gitRepo.get.get)).foreach(coCommit => {
          println(s"${coCommit.count} ${coCommit.files._1} ${coCommit.files._2}")
        })
      }
      case Some(Conf.topByTag) if (Conf.topByTag.gitRepo.isSupplied && Conf.topByTag.tag.isSupplied) => {
        DB.topByTag(Repository(Conf.topByTag.gitRepo.get.get), Conf.topByTag.tag.get.get).foreach(fileNCount => {
          println(s"${fileNCount._1.path} ${fileNCount._2}")
        })
      }
      case Some(Conf.clean) => DB.clean()
      case Some(Conf.load) => {
        val repo = Repository(Conf.load.gitRepo.get.get)
        DB.createRepo(repo)
        val commits = repo.latestCommits(Conf.load.maxCommits.get.get)
        val cardsForCommits : Map[Commit,Card] = if(Conf.load.mingleApiUrl.isSupplied) {
          new Mingle(baseUrl=Conf.load.mingleApiUrl.get.get).cardsFor(commits)
        } else {
          Map()
        }
        commits.foreach( commit => {
          if (commit.fileChanges.size<=20) {
            val cardOpt = cardsForCommits.get(commit)
            logger.info(s"Loading commit ${commit.hash} with ${commit.fileChanges.size} file changes. card=${cardOpt}")
            DB.create(repo, commit, cardOpt)
          } else {
            logger.info(s"Skipping commit ${commit.hash} with too many file changes (${commit.fileChanges.size}).")
          }
        })
      }
      case _ => Conf.printHelp()
    }
  }

}
