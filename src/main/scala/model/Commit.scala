package model

case class Commit(hash:String, time:Int, message:String, fileChanges:Seq[FileChange]) {
  private val HashTagPattern  = "(#\\S+)".r
  lazy val hashTags = HashTagPattern.findAllIn(message).toSeq
}

case class FileChange(changeType:String, newPath:String, oldPath:String)

case class CoCommit(files:(String,String), count:Int)

case class SourceCodeFile(path:String)

