package neo4j

import org.anormcypher._
import git.Repository
import org.slf4j.LoggerFactory
import model.{SourceCodeFile, Card, CoCommit, Commit}

object DB {

  val logger = LoggerFactory.getLogger(this.getClass)

  def create(repo:Repository, commit:Commit, card:Option[Card]=None) {
    createCommit(commit, repo, card)
    createFileChanges(commit)
  }

  def createRepo(repo: Repository) {
    Cypher( """MERGE ( r:Repository { dir:"%s" }) return r""".format(repo.dir)).apply()
  }

  def topCoCommits(repo: Repository): Seq[CoCommit] = {
    for (row <- Cypher( """MATCH (file1)<-[r:CHANGED_WITH]-(file2) return r.count,file1.path,file2.path ORDER BY r.count DESC LIMIT 100""").apply()) yield {
      CoCommit((row[String]("file1.path"), row[String]("file2.path")), row[Int]("r.count"))
    }
  }

  def topByTag(repo: Repository, tag:String): Seq[(SourceCodeFile, Int)] = {
    for (row <- Cypher( s"""MATCH (f:File)-[r:CHANGED_IN]->(c:Commit), c-[:TAGGED_WITH]->(t:Tag) WHERE t.tag='${tag.toLowerCase}' return f.path,count(f) ORDER BY count(f) DESC LIMIT 40""").apply()) yield {
      (SourceCodeFile((row[String]("f.path"))), row[Int]("count(f)"))
    }
  }

  def buggiest(repo: Repository): Seq[(String,Int)] = {
    for (row <- Cypher( """MATCH (f:File)-[:CHANGED_IN]->(c:Commit)-[:TAGGED_WITH]->(t:Bug) return f.path,count(f.path)""").apply()) yield {
      (row[String]("f.path"), row[Int]("count(f.path)"))
    }
  }

  def clean() {
    Cypher("start r=relationship(*) delete r").execute()
    Cypher("MATCH n DELETE n").execute()
  }

  private def createCommit(commit:Commit, repo:Repository, card:Option[Card]) {
    Cypher("""MERGE ( c:Commit { hash:"%s", time:%s, message:"%s" })""".format(commit.hash, commit.time, commit.message)).execute()
    card.map(card=>{
      val cardNodeLabel = card.cardType.replaceAll(" ", "")
      Cypher(s"""MERGE ( c:${cardNodeLabel} { id:"${card.id}", title:"${card.title}" })""").execute()
      Cypher(s"""MATCH (co:Commit),(c:${cardNodeLabel}) WHERE c.id = '${card.id}' AND co.hash='${commit.hash}' CREATE UNIQUE co-[:TAGGED_WITH]->c""").execute()
    })
    commit.hashTags.foreach(ht => {
      Cypher("""MERGE ( t:Tag { tag:"%s"})""".format(ht.toLowerCase)).execute()
      Cypher(s"""MATCH (co:Commit),(t:Tag) WHERE t.tag = '${ht.toLowerCase}' AND co.hash='${commit.hash}' CREATE UNIQUE co-[:TAGGED_WITH]->t""").execute()
    })
    Cypher("""MATCH (c:Commit),(r:Repository) WHERE c.hash = '%s' AND r.dir='%s' CREATE UNIQUE c-[:COMMITTED_TO]->r""".format(commit.hash, repo.dir)).execute()
  }

  private def createFileChanges(commit:Commit) {
    commit.fileChanges.foreach( fileChange => {
      fileChange.newPath match {
        case "/dev/null" =>
        case filePath => {
          Cypher("""MERGE ( f:File { path:"%s" })""".format(filePath)).execute()
          Cypher("""MATCH (f:File),(c:Commit) WHERE f.path = '%s' AND c.hash='%s' CREATE UNIQUE f-[:CHANGED_IN]->c""".format(filePath, commit.hash)).execute()
        }
      }
    })
    commit.fileChanges.map(_.newPath).filterNot(_=="/dev/null").combinations(2).foreach(pair => {
      Cypher("""MATCH (f1:File),(f2:File) WHERE f1.path = '%s' AND f2.path='%s' CREATE UNIQUE f1-[sc:CHANGED_WITH]->f2 SET sc.count = coalesce(sc.count,0) + 1 """.format(pair(0), pair(1))).execute()
    })
  }

}
