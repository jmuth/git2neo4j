package git

import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import java.io.File
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.{RawTextComparator, DiffFormatter}
import org.eclipse.jgit.util.io.DisabledOutputStream
import org.eclipse.jgit.revwalk.RevCommit
import scala.collection.JavaConversions._
import org.eclipse.jgit.lib.ObjectId
import model.{FileChange, Commit}

class Repository(repo:org.eclipse.jgit.lib.Repository) {

  val dir = repo.getDirectory.getAbsolutePath

  def latestCommits(maxCommits:Int) : Seq[Commit] = {
    for (commit <- new Git(repo).log().setMaxCount(maxCommits).call().iterator()) yield {
      Commit(ObjectId.toString(commit.getId), commit.getCommitTime, commit.getShortMessage, getFileChanges(commit))
    }
  }.toSeq

  private def getFileChanges(commit:RevCommit) : Seq[FileChange] = {
    val df = new DiffFormatter(DisabledOutputStream.INSTANCE)
    df.setRepository(repo)
    df.setDiffComparator(RawTextComparator.DEFAULT)
    df.setDetectRenames(true)
    df.scan(commit.getParent(0).getTree(), commit.getTree()).map(de=> {
      FileChange(de.getChangeType.toString, de.getNewPath, de.getOldPath)
    })
  }
}

object Repository {
  def apply(dir:String) = {
    val repoBuilder = new FileRepositoryBuilder
    new Repository(repoBuilder.setGitDir(new File(dir)).build())
  }
}
