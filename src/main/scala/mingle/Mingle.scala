package mingle

import scala.xml.XML
import org.slf4j.LoggerFactory
import model.{Commit, Card}
import org.apache.http.impl.client.{DefaultHttpClient, BasicCredentialsProvider}
import org.apache.http.auth.{UsernamePasswordCredentials, AuthScope}
import org.apache.http.client.methods.HttpGet
import org.apache.http.util.EntityUtils
import org.apache.http.params.{CoreProtocolPNames, BasicHttpParams}

class Mingle(httpClient:DefaultHttpClient=new DefaultHttpClient(), baseUrl:String="http://mingle.tools.springer-sbm.com:8080/api/v2", project:String="casper") {

  private val logger = LoggerFactory.getLogger(this.getClass)
  private val credsProvider = new BasicCredentialsProvider()
  credsProvider.setCredentials(
    new AuthScope("mingle.tools.springer-sbm.com", 8080),
    new UsernamePasswordCredentials("john", "john")
  )
  private val params = new BasicHttpParams()
  params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8")

  private val MingleCardHashTagPattern = "#(\\d+)".r

  def cardsFor(commits:Seq[Commit]) : Map[Commit,Card] = {
    val possibleMingleCardIds = commits.flatMap(_.hashTags).distinct.filter(MingleCardHashTagPattern.pattern.matcher(_).matches()).map(_.replaceFirst("#", ""))
    val mingleCards = possibleMingleCardIds.map(id => findCard(id.toInt)).filter(_.isDefined).map(_.get)
    mingleCards.map(mc => {
      for (commit <- commits.filter(_.hashTags.contains("#%d".format(mc.id)))) yield {
        commit -> mc
      }
    }).flatten
  }.toMap

  private def findCard(id:Int) : Option[Card] = {
    httpClient.setParams(params)
    httpClient.setCredentialsProvider(credsProvider)
    val cardUrl = s"${baseUrl}/projects/${project}/cards/${id}.xml"
    try {
      val httpGet = new HttpGet(cardUrl)
      val response = httpClient.execute(httpGet)
      val cardString = EntityUtils.toString(response.getEntity)
      val cardXml = XML.loadString(cardString)
      Some(Card(id, (cardXml \ "card_type" \ "name").text, (cardXml \ "name").text))
    } catch {
      case t:Throwable => {
        logger.error(s"Error getting card ${id} from Mingle.", t)
        None
      }
    }
  }

}

